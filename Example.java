import conesus.*;
import conesus.log.*;


class Example {
	public static void main(String[] args) {
		Conesus con = new Conesus("/path/to/repository");

		// -----------------------------------------------------------------------------
		LogFile output = con.logFiles()[2];

		System.out.println("         Hash:  " + output.hash);
		System.out.println("       Status:  " + output.status);
		System.out.println("        Score:  " + output.score);
		System.out.println("         File:  " + output.file);
		System.out.println("Previous file:  " + output.previousFile);

		// -----------------------------------------------------------------------------
		// System.out.println(con.commitForTag("1.6.22"));

		// -----------------------------------------------------------------------------
		// LogEntry[] output = con.log();

		// for(LogEntry entry : sample.log()) {
		// 	System.out.println("");
		// 	System.out.println("   Hash:  " + entry.hash);
		// 	System.out.println("Message:  " + entry.message);
		// }

		// -----------------------------------------------------------------------------
		// for(String entry : con.rawLog()) {
		// 	System.out.println(entry);
		// }

		// -----------------------------------------------------------------------------
		// String[] commits = con.commits();
	}
}
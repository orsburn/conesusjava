package conesus;

import java.lang.Process;
import java.lang.Runtime;
import java.util.Objects;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import conesus.log.LogEntry;
import conesus.log.LogFile;


public class Conesus {
	public String repository;

	public Conesus(String repository) {
		this.repository = repository;
		// System.out.println(repository);
	}

	public Conesus() {
		this(null);
	}


	public LogEntry[] log() {
		String[] log = this.run("git log --all --decorate --oneline").split("\\n");

		LogEntry[] results = new LogEntry[log.length];
		int resultsIndex = 0;

		for(String entry : log) {
			int splitHere = entry.indexOf(" ");

			LogEntry resultsInstance = new LogEntry();

			resultsInstance.hash = entry.substring(0, splitHere);
			resultsInstance.message = entry.substring(splitHere + 1);  // Plus 1 to skip the space we split on.

			results[resultsIndex ++] = resultsInstance;
		}

		return results;
	}


	public String[] rawLog() {
		return this.run("git log --all --decorate --oneline").split("\\n");
	}


	public LogFile[] logFiles() {
		String[] log = run("git log --all --name-status --oneline --pretty=format:\"%H\"").split("\\n");
		LogFile[] results = new LogFile[log.length];
		String hash = null;
		int resultsIndex = 0;

		for(String entry : log) {
			LogFile logFile = new LogFile();
			String[] fields = entry.split("\\t");

			if(fields.length >= 2) {
				logFile.hash = hash;
				logFile.status = fields[0].charAt(0);

				if((fields[0].charAt(0) == 'R') || (fields[0].charAt(0) == 'C')) {
					logFile.status = fields[0].charAt(0);
					logFile.score = Integer.parseInt(fields[0].substring(1));
				}

				if(fields.length == 3) {
					logFile.file = fields[2];
					logFile.previousFile = fields[1];  // If the file was renamed.
				}
				else {
					logFile.file = fields[1];
				}

				results[resultsIndex ++] = logFile;
			}
			else if(fields[0] != "") {
				hash = fields[0];
			}
		}

		return results;
	}


	public String[] commits() {
		return this.run("git log --all --decorate --oneline --pretty=format:\"%H\"").split("\\n");
	}


	public String commitForTag(String tag) {
		/*
		This method works the way it does because there no good way to handle the case
		where the tag being sought doesn't exist.  Any such checks would likely require
		multiple calls to git on the system.  It's probably faster to just parse some
		git output.`

		Examples that have been tried:

			git tag -v [TAG]
			git rev-parse [TAG]
			git rev-list -n [TAG]
		*/
		String[] listing = run("git show-ref --tags --dereference").split("\\n");
		String outputCommitId = null;

		for(String entry : listing) {
			String[] pieces = entry.split(" ");
			String commitId = pieces[0];
			String tagRef = pieces[1];

			String tagCheck = tagRef.substring(tagRef.lastIndexOf('/') + 1);

			/*
			The entire list is scanned rather than tring to be clever about accommodating
			annotated tags.  Based on the listing from git log, it looks like the hash that
			should be output is the one with the annotation if one exists.
			*/
			if(Objects.equals(tagCheck, tag) || Objects.equals(tagCheck, tag + "^{}")) {  // BUG  This will likely need to be updated to allow content betweeb the curly braces.
				outputCommitId = commitId;
			}
		}

		return outputCommitId;
	}


	private String run(String command) {
		File repository = new File(this.repository);

		try {
			Process proc = Runtime.getRuntime().exec(command, null, repository);
			InputStream input = proc.getInputStream();
			String results = new String();
			int currentChar;

			while((currentChar = input.read()) != -1){
				results = results + (char) currentChar;
			}

			input.close();

			String output = new String(results).trim();

			return output;
		} catch(IOException e) {
			System.err.println("Caught IOException:  " + e.getMessage());
		}

		return new String();
	}
}
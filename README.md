*The actual readme is found in the **README** folder.  The more consistent nature of HTML documents over Markdown documents was favored for an elaborate readme file.*

# Origin

The idea for this project was originally hatched from an idea for creating scripts to automate the deployment of applications directly from Git repositories.  There is no real API support for Git, so a PHP wrapper around Git was born.

This project is a Java implementation of that wrapper library.

# Example

The **/Example.java** file can be experimented with to see the types of output that's generated.

Run the example like:

```
javac @.compileExample
javac @.run
```

***NOTE:**  The classpath separator indicated with the -cp, -classpath, or --class-path options will need to be a semicolon on Windows machines.  This can be seen for example in the **/.compileExample** file*